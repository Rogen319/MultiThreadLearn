package semaphoreexample;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

//为什么跑不完？第一次循环以后就结束了？
//解决：除了一开始的有跑道，其它的都没有跑道，track为null，一开始没有任何打印信息，所以看不到效果

//跑完以后，为啥还会在那运行那么久的时间？
public class OrdinaryDemo {
	static class Student implements Runnable{
		private int num;
		private OrdinaryPlayground playground;

		public Student(int num, OrdinaryPlayground playground) {
			this.num = num;
			this.playground = playground;
		}

		@Override
		public void run() {
			try {
				OrdinaryPlayground.Track track = playground.getTrack();
				if(track != null) {
					System.out.println("学生" + num + "在" + track.toString() + "上跑步");
					TimeUnit.SECONDS.sleep(2);
					System.out.println("学生" + num + "释放" + track.toString());
					//释放跑道
					playground.releaseTrack(track);
				}
				else {
					System.out.println("学生" + num + "没有跑道可使用");
				}
			}catch (Exception e) {
				e.printStackTrace();
			}

		}

	}

	public static void main(String[] args) {
		Executor executor = Executors.newCachedThreadPool();
		OrdinaryPlayground playground = new OrdinaryPlayground();
		for(int i = 0; i < 100; i++) {
			executor.execute(new Student(i + 1, playground));
		}
	}

}
