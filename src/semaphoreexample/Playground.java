package semaphoreexample;

import java.util.concurrent.Semaphore;

public class Playground {
	
	static class Track{
		private int num;
		
		public Track(int num) {
			this.num = num;
		}

		public int getNum() {
			return num;
		}

		public void setNum(int num) {
			this.num = num;
		}

		@Override
		public String toString() {
			return "Track{num=" + this.num + "}";
		}
	}
	
	private Track[] tracks = {new Track(1),new Track(2),new Track(3),new Track(4),new Track(5)};
//	private volatile boolean[] used = new boolean[5];
	private boolean[] used = new boolean[5];
	
	private Semaphore semaphore = new Semaphore(5,true);
	
	//获取一个跑道
	public Track getTrack() throws InterruptedException {
		semaphore.acquire();
		for(int i = 0; i < used.length; i++) {
			if(!used[i]) {
				used[i] = true;
//				System.out.println("return the track num is " + i);
				return tracks[i];
			}
		}
		return null;
	}
	
	//返回一个跑道
	public void releaseTrack(Track track) {
		for(int i = 0; i < tracks.length; i++) {
			if(tracks[i] == track) {
				used[i] = false;
				semaphore.release();
			}
		}
	}

}
