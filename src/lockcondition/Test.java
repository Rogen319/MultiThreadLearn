package lockcondition;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Test {

	public static void main(String[] args) {

		try {
            Lock lock = new ReentrantLock(); 
            Condition con = lock.newCondition();
            
            ThreadA a = new ThreadA(lock,con);
            a.start();

            Thread.sleep(50);

            ThreadB b = new ThreadB(lock,con);
            b.start();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
	}

}
