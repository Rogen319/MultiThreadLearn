package lockcondition;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;

public class ThreadB extends Thread {
	private Lock lock;
	private Condition condition;
	
	public ThreadB(Lock lock,Condition condition) {
		this.lock = lock;
		this.condition = condition;
	}

	@Override
	public void run() {
		lock.lock();
		try {
			for(int i = 0; i < 10; i++) {
				MyList.add();
				if(MyList.size() == 5) {
					condition.signal();
					System.out.println("已发出了通知");
				}
				System.out.println("添加了" + (i + 1) + "个元素!");
				Thread.sleep(1000);
			}
		} catch (InterruptedException  e) {
			e.printStackTrace();
		}finally {
			lock.unlock();
		}
	}
	
	
}
