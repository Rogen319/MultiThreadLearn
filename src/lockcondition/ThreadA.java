package lockcondition;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;

public class ThreadA extends Thread {
	private Lock lock;
	private Condition condition;
	
	public ThreadA(Lock lock,Condition condition) {
		this.lock = lock;
		this.condition = condition;
	}

	@Override
	public void run() {
		lock.lock();	
		try {
			if(MyList.size() != 5) {
				System.out.println("Wait begin " + System.currentTimeMillis());
				condition.await();
				System.out.println("Wait end " + System.currentTimeMillis());
			}
		} catch (InterruptedException  e) {
			e.printStackTrace();
		}finally {
			lock.unlock();
		}
	}
	
	
}
