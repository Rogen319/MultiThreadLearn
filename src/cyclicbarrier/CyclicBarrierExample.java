package cyclicbarrier;

import java.util.concurrent.CyclicBarrier;

public class CyclicBarrierExample {

	public static void main(String[] args) {
		CyclicBarrier barrier = new CyclicBarrier(4, new Runnable() {
			
			@Override
			public void run() {
				System.out.println("��ǰ�߳�"+Thread.currentThread().getName());				
			}
		});
		
		for(int i = 0; i < 4; i++) {
			new CyclicBarrierThread(barrier).start();
		}
		
		for(int i = 0; i < 4; i++) {
			new CyclicBarrierThread(barrier).start();
		}
	}

}
