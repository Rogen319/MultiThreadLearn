package fooson;

public class Foo {
	int i = 1;
	static {
		System.out.println("Father init!");
	}
	
	void f() {
		System.out.println("Father f");
	}

}
