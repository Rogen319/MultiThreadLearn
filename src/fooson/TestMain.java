package fooson;

public class TestMain {

	public static void main(String[] args) {
		Foo foo = new Son();
		System.out.println(foo.i);
		foo.f();
		System.out.println(String.format("The class is %s", foo.getClass()));
		System.out.println(String.format("The class loader is %s", foo.getClass().getClassLoader()));
	}

}
