package fooson;

public class Son extends Foo {
	int i = 2;
	static {
		System.out.println("Son init!");
	}
	
	void f() {
		System.out.println("Son f");
	}

}
