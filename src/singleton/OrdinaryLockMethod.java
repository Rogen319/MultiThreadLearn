package singleton;

public class OrdinaryLockMethod {
	private static OrdinaryLockMethod ordinaryLockMethod;

	private OrdinaryLockMethod() {

	}

	public synchronized static OrdinaryLockMethod getInstance() {
		if(ordinaryLockMethod == null)
			ordinaryLockMethod = new OrdinaryLockMethod();

		return ordinaryLockMethod;
	}
}
