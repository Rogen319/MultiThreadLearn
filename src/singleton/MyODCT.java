package singleton;

public class MyODCT extends Thread {

	@Override
	public void run() {
		System.out.println("The singleton object address is " + OrdinaryDoubleCheckedMethod.getInstance());
	}

}
