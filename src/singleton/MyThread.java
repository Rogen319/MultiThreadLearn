package singleton;

public class MyThread extends Thread {

	@Override
	public void run() {
		System.out.println("The singleton object address is " + OrdinarySingletonMethod.getInstance());
	}

}
