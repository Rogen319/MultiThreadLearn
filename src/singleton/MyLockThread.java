package singleton;

public class MyLockThread extends Thread {

	@Override
	public void run() {
		System.out.println("The singleton object address is " + OrdinaryLockMethod.getInstance());
	}

}
