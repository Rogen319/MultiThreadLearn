package singleton;

public class OrdinarySingletonMethod {
	private static OrdinarySingletonMethod ordinaryMethod;
	
	private OrdinarySingletonMethod() {
		
	}
	
	public static OrdinarySingletonMethod getInstance() {
		if(ordinaryMethod == null)
			ordinaryMethod = new OrdinarySingletonMethod();
		
		return ordinaryMethod;
	}
}
