package singleton;

public class HungryMethod {
	private static HungryMethod hungryMethod = new HungryMethod();

	private HungryMethod() {
		
	}
	
	public static HungryMethod getInstance() {
		return hungryMethod;
	}
}
