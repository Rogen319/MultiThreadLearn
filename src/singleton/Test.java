package singleton;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class Test {

	public static void main(String[] args) {

		//Test single thread
		//		for(int i = 0; i < 10; i++) {
		//			System.out.println("The singleton object address is " + OrdinarySingletonMethod.getInstance());
		//			System.out.println("The ordinary object address is " + new Object());
		//		}

		Executor executor = Executors.newCachedThreadPool();

		//Test multiple threads with oridinary method
		//It does't work
		//		for(int i = 0; i < 10; i++) {
		//			executor.execute(new MyThread());
		//		}

		//Test multiple threads with lock method
		//It works
		//		for(int i = 0; i < 10; i++) {
		//			executor.execute(new MyLockThread());
		//		}

		//Test multiple threads with ordinary double check method
		//Most of the time, it works
//		for(int i = 0; i < 10; i++) {
//			executor.execute(new MyODCT());
//		}

		//Test multiple threads with hungry method
		//It works
		for(int i = 0; i < 10; i++) {
			executor.execute(new MyHungryThread());
		}
	}

}
