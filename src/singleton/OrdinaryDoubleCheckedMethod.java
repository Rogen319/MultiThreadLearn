package singleton;

public class OrdinaryDoubleCheckedMethod {
	private static OrdinaryDoubleCheckedMethod ordinaryDoubleCheckedMethod;

	private OrdinaryDoubleCheckedMethod() {

	}

	public static OrdinaryDoubleCheckedMethod getInstance() {

		if(ordinaryDoubleCheckedMethod == null) {
			synchronized (OrdinaryDoubleCheckedMethod.class) {
				if(ordinaryDoubleCheckedMethod == null)
					ordinaryDoubleCheckedMethod = new OrdinaryDoubleCheckedMethod();
			}
		}	

		return ordinaryDoubleCheckedMethod;
	}
}
