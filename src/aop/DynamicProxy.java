package aop;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

public class DynamicProxy implements InvocationHandler {
	private Object target;
	
	//绑定委托对象并返回一个代理类
	public Object bind(Object target) {
		this.target = target;
		return Proxy.newProxyInstance(target.getClass().getClassLoader(),
				target.getClass().getInterfaces(), this);
	}

	@Override
	public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
		System.out.println("方法前执行！");
		Object result = null;
//		result = method.invoke(proxy, args);
		result = method.invoke(target, args);
		System.out.println("方法后执行！");
		return result;
	}

}
