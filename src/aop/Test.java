package aop;

public class Test {

	public static void main(String[] args) {
		ITalk talker = (ITalk)new DynamicProxy().bind(new PeopleTalk("AOP", "18"));
		talker.talk("业务说明：");
		
		PeopleTalk peopleTalk = (PeopleTalk)new DynamicProxy().bind(new PeopleTalk("AOP", "18"));
		peopleTalk.talk("业务说明：");
	}

}
