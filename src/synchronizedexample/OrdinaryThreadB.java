package synchronizedexample;

public class OrdinaryThreadB extends Thread {

	private OrdinaryObject object;
	
	public OrdinaryThreadB(OrdinaryObject object) {
		this.object = object;
	}
	
	@Override
	public void run() {
		object.methodB();
	}
}
