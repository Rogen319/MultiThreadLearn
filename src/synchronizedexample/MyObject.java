package synchronizedexample;

import java.util.concurrent.TimeUnit;

public class MyObject {
	
	synchronized public void methodA() {
		try {
			System.out.println("methodA starts to execute!");
			TimeUnit.SECONDS.sleep(2);
			System.out.println("methodA ends");
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	synchronized public void methodB() {
		try {
			System.out.println("methodB starts to execute!");
			TimeUnit.SECONDS.sleep(2);
			System.out.println("methodB ends");
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

}
