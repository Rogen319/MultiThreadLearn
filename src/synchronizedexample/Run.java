package synchronizedexample;

public class Run {

	public static void main(String[] args) {
		MyObject object = new MyObject();
		ThreadA tA = new ThreadA(object);
		ThreadB tB = new ThreadB(object);
		tA.start();
		tB.start();
	}

}
