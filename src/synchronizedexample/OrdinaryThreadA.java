package synchronizedexample;

public class OrdinaryThreadA extends Thread {

	private OrdinaryObject object;
	
	public OrdinaryThreadA(OrdinaryObject object) {
		this.object = object;
	}
	
	@Override
	public void run() {
		object.methodA();
	}
}
