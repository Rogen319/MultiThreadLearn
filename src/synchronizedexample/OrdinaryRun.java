package synchronizedexample;

public class OrdinaryRun {

	public static void main(String[] args) {
		OrdinaryObject object = new OrdinaryObject();
		OrdinaryThreadA tA = new OrdinaryThreadA(object);
		OrdinaryThreadB tB = new OrdinaryThreadB(object);
		tA.start();
		tB.start();
	}

}
