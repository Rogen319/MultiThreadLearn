package pipe;

import java.io.IOException;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;

public class TestPipe2 {

	public static void main(String[] args) {
		PipedOutputStream pos = new PipedOutputStream();
		PipedInputStream pis = new PipedInputStream();
		try {
			pos.connect(pis);
		} catch (IOException e) {
			e.printStackTrace();
		}
		//注意这里new对象的方式：内部类的声明方式
		TestPipe2 pipe = new TestPipe2();
		Producer p = pipe.new Producer(pos);
		MyConsumer mc = pipe.new MyConsumer(pis);

		p.start();
		mc.start();
	}
	
	/** 
	 * 生产者线程(与一个管道输入流相关联) 
	 *  
	 */  
	class Producer extends Thread {  
		private PipedOutputStream pos;  

		public Producer(PipedOutputStream pos) {  
			this.pos = pos;  
		}  

		public void run() {  
			int i = 0;  
			try {  
				while(true)  
				{  
					this.sleep(3000); 
					System.out.println("Producer produces " + i);
					pos.write(i);  
					i++;  
				}  
			} catch (Exception e) {  
				e.printStackTrace();  
			}  
		}  
	}  

	/** 
	 * 消费者线程(与一个管道输入流相关联) 
	 *  
	 */  
	class MyConsumer extends Thread {  
		private PipedInputStream pis;  

		public MyConsumer(PipedInputStream pis) {  
			this.pis = pis;  
		}  

		public void run() {  
			try {  
				while(true)  
				{  
					System.out.println("Consumer1 consumes: "+pis.read());  
				}  
			} catch (IOException e) {  
				e.printStackTrace();  
			}  
		}  
	}

	  
}

