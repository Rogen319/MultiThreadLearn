package countdownlatchexa;

import java.util.concurrent.CountDownLatch;

public class CountDownLatchThread extends Thread {
	CountDownLatch countDownLatch;
	
	public CountDownLatchThread(CountDownLatch countDownLatch) {
		this.countDownLatch = countDownLatch;
	}

	@Override
	public void run() {
		try {
			System.out.println("Thread" + this.getName() + " begins!");
			Thread.sleep(1000);
			countDownLatch.countDown();
			System.out.println("Thread" + this.getName() + " ends!");
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

}
