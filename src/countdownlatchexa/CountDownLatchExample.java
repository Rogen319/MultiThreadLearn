package countdownlatchexa;

import java.util.concurrent.CountDownLatch;

public class CountDownLatchExample {

	public static void main(String[] args) {
		System.out.println("主线程开始执行");
		CountDownLatch countDownLatch = new CountDownLatch(2);
		CountDownLatchThread thread = new CountDownLatchThread(countDownLatch);
		CountDownLatchThread thread2 = new CountDownLatchThread(countDownLatch);
		thread.start();
		thread2.start();
		try {
			countDownLatch.await();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println("主线程结束");
	}
}
